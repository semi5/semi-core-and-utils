# Scripting Engine for Melvor Idle

## Installation Guide

## Contributors

## Reporting Bugs

## Goal of the Software

This software was made to unify many Melvor automation and QOL scripts, including my own, into one easy-to-use platform with a UI that mirrors the game, without worrying about compatibility or maintaining individual userscripts.

## Thanks Again

This project has been maintained primarily by the community. Thanks to all the helpful developers out there! And thanks to all the users, it's all for you!

