export function setup({ api, settings, getResourceUrl, onInterfaceReady }) {
	const id = 'semi-core';
	const spellSelectionKeys = ['attack', 'curse', 'aurora'];

	// Script specific functions
	const log = (submod_name, ...message) => {
		const debugLogging = getSubmodSettings('SEMI Core', 'debug-logging');
		if (debugLogging) {
			console.log(`[SEMI] [${submod_name}]`, ...message);
		}
	};

	/**
	 * Add a custom menu item into the Mod Settings area, similar to how a mod with settings works.
	 * This allows a custom callback instead of spawning the settings modal for the mod.
	 * @param {string} itemName Menu Label Name
	 * @param {string} itemIcon Icon Image URL
	 * @param {function} itemCallback Function Callback
	 */
	const addSideBarModSetting = (itemName, itemIcon, itemCallback) => {
		const modSettingsItem = sidebar.category('Modding').item('Mod Settings');
		const existingSubitems = modSettingsItem.subitems();
		let before;
		for (const subitem of existingSubitems) {
			if (itemName < subitem.id) {
				before = subitem.id;
				break;
			}
		}
		modSettingsItem.subitem(itemName, {
			name: createElement('span', {
				children: [
					createElement('img', {
						classList: ['mr-2'],
						attributes: [
							['src', itemIcon],
							['height', '24'],
							['width', '24'],
						],
					}),
					itemName,
				],
			}),
			before,
			onClick: () => itemCallback(),
		});
	};

	/**
	 * Build a barebones overlay modal to display content.
	 * This returns an object containing references to the main modal,
	 * the title element, and the content element.
	 * @param {string} modalID Modal ID
	 * @param {string} modalTitle Default Modal Window Title
	 * @param {string} modalContent Default Modal Window Content
	 */
	const buildModal = (modalID, modalTitle = '', modalContent = '') => {
		if ($(`#modal-${modalID}`).length) {
			$(`#modal-${modalID}`).modal('hide');
			$(`#modal-${modalID}`).remove();
		}

		$('#modal-privacy').after(
			`<div class="modal semi-modal" id="modal-${modalID}" tabindex="-1" role="dialog" aria-labelledby="modal-block-normal" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <div class="block block-themed block-transparent mb-0">
                        <div class="block-header bg-primary-dark">
                            <h3 class="block-title">${modalTitle}</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                    <i class="fa fa-fw fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-container">
                            ${modalContent}
                        </div>
                    </div>
                </div>
            </div>
        </div>`);

		const modal = $(`#modal-${modalID}`);
		const blockTitle = modal.find('.block-title');
		const blockContainer = modal.find('.block-container');

		return { modal, blockTitle, blockContainer };
	};

	// Equipment
	const equipmentSlotHasItem = (setID, slotID, item) => game.combat.player.equipmentSets[setID].equipment.equippedItems[slotID].item == item;

	/**
	 * Get a snapshot of the players current equipment items and quantities.
	 */
	const equipmentSnapshot = () => {
		const data = {_spells: {}};

		// Equipment
		game.combat.player.equipment.equippedArray.forEach(index => {
			data[index.slot.id] = { item: index.item, quantity: index.quantity }
		});

		// Spells
		spellSelectionKeys.forEach(key => {
			if (game.combat.player.spellSelection[key] != undefined)
				data._spells[key] = game.combat.player.spellSelection[key];
		});

		// Prayers
		data._prayers = [...game.combat.player.activePrayers];

		return data;
	};

	/**
	 * Get a difference in current player equipment as compared to a provided snapshot.
	 * @param {Object} snapshot As generated from `equipmentSnapshot()`
	 * @param {boolean} compareQty Include differences in item quantity.
	 */
	const equipmentDifferences = (snapshot, compareQty = false) => {
		if (!snapshot)
			return null;

		const data = {_spells: {}};

		// Equipment
		game.combat.player.equipment.equippedArray.forEach(index => {
			const slotID = index.slot.id;

			let hasDifference = (snapshot[slotID].item != index.item);

			if (compareQty && snapshot[slotID].quantity != index.quantity)
				hasDifference = true;

			if (hasDifference) {
				data[slotID] = {
					old: { item: snapshot[slotID].item, quantity: snapshot[slotID].quantity },
					new: { item: index.item, quantity: index.quantity }
				};
			}
		});

		// Spells
		spellSelectionKeys.forEach(key => {
			if (game.combat.player.spellSelection[key] != snapshot._spells[key])
				data._spells[key] = {
					old: snapshot._spells[key],
					new: game.combat.player.spellSelection[key]
				};
		});

		// Prayers
		let hasPrayerDifference = game.combat.player.activePrayers.size != snapshot._prayers.length;
		let currentPrayers = [...game.combat.player.activePrayers];

		if (!hasPrayerDifference && snapshot._prayers.length > 0)
			hasPrayerDifference = currentPrayers.filter(x => snapshot._prayers.indexOf(x) == -1).length != 0;

		if (hasPrayerDifference)
			data._prayers = {
				old: snapshot._prayers,
				new: currentPrayers
			};

		return data;
	};

	/**
	 * Equip a item to a slot if valid, or unequip if empty.
	 * @param {number} setID Equipment Set
	 * @param {string} slotID Slot ID
	 * @param {Object} slotItem Objecct containing a item and quantity
	 */
	const equipmentEquipSlot = (setID, slotID, slotItem) => {
		const slot = game.equipmentSlots.getObjectByID(slotID);
		if (!slot)
			return false;

		// Slot is Empty
		if (slotItem.item == game.emptyEquipmentItem) {
			if (!equipmentSlotHasItem(setID, slot.id, slotItem.item) && !game.combat.player.unequipItem(setID, slot)) {
				return false;
			}
		}
		// Slot has Item, but is filled from another slot. (2H Weapons, etc)
		else if (slotItem.quantity <= 0) {
			return true;
		}
		// Slot has Item
		else if (!equipmentSlotHasItem(setID, slot.id, slotItem.item)) {
			if (game.bank.getQty(slotItem.item) <= 0 || !game.combat.player.equipItem(slotItem.item, setID, slot, slotItem.quantity)) {
				return false;
			}
		}
		return true;
	};

	/**
	 * Attempt to restore a players equipment to a provided snapshot.
	 * Returns `true` if all equipment was equipped, or `false` if snapshot is null or
	 * an item was unable to be equipped.
	 * @param {Object} snapshot As generated from `equipmentSnapshot()`
	 */
	const equipmentSnapshotRestore = (snapshot) => {
		if (!snapshot)
			return false;

		const player = game.combat.player;

		// Equipment
		const setID = player.selectedEquipmentSet;
		let equipFail = true;
		player.equipment.equippedArray.forEach(index => {
			if (snapshot[index.slot.id] && !equipmentEquipSlot(setID, index.slot.id, snapshot[index.slot.id])) {
				equipFail = true;
			}
		});

		// Spells
		if (snapshot._spells.attack != undefined) // Attack
			player.selectAttackSpell(snapshot._spells.attack);

		if (snapshot._spells.curse != player.spellSelection.curse) {
			if (snapshot._spells.curse == undefined && player.spellSelection.curse != undefined) // Curse
				player.toggleCurse(player.spellSelection.curse); // Unselect
			else if (snapshot._spells.curse != undefined)
				player.toggleCurse(snapshot._spells.curse); // Change Select
		}
		
		if (snapshot._spells.aurora != player.spellSelection.aurora) {
			if (snapshot._spells.aurora == undefined && player.spellSelection.aurora != undefined) // Aurora
				player.toggleAurora(player.spellSelection.aurora); // Unselect
			else if (snapshot._spells.aurora != undefined)
				player.toggleAurora(snapshot._spells.aurora); // Change Select
		}

		// Prayers
		if (snapshot._prayers != undefined) {
			player.activePrayers.forEach(val => {
				player.togglePrayer(val); // Clear prayers.
			});
			snapshot._prayers.forEach(val => {
				player.togglePrayer(val); // Restore prayers.
			});
		}

		return equipFail;
	};

	/**
	 * Attempt to restore a players equipment to the old equipment in a snapshot difference.
	 * Returns `true` if all equipment was equipped, or `false` if snapshot is null or
	 * an item was unable to be equipped.
	 * @param {Object} snapshot As generated from `equipmentDifferences()`
	 */
	const equipmentDifferencesRestore = (differences) => {
		if (!differences)
			return false;

		const player = game.combat.player;

		// Equipment
		const setID = player.selectedEquipmentSet;
		let equipFail = true;
		player.equipment.equippedArray.forEach(index => {
			if (differences[index.slot.id] && !equipmentEquipSlot(setID, index.slot.id, differences[index.slot.id].old)) {
				equipFail = true;
			}
		});

		// Spells
		if (differences._spells.attack != undefined) // Attack
			player.selectAttackSpell(differences._spells.attack.old);

		if (differences._spells.curse != undefined) { // Curse
			if (differences._spells.curse.old == undefined && player.spellSelection.curse != undefined)
				player.toggleCurse(player.spellSelection.curse); // Unselect
			else if (differences._spells.curse.old != undefined)
				player.toggleCurse(differences._spells.curse.old); // Change Select
		}

		if (differences._spells.aurora != undefined) { // Aurora
			if (differences._spells.aurora.old == undefined && player.spellSelection.aurora != undefined)
				player.toggleAurora(player.spellSelection.aurora); // Unselect
			else if (differences._spells.aurora.old != undefined)
				player.toggleAurora(differences._spells.aurora.old); // Change Select
		}

		// Prayers
		if (differences._prayers != undefined && differences._prayers.old != undefined) {
			player.activePrayers.forEach(val => {
				player.togglePrayer(val); // Clear prayers.
			});
			differences._prayers.old.forEach(val => {
				player.togglePrayer(val); // Restore Prayers
			});
		}

		return equipFail;
	};

	// Game utils functions
	const isItemFound = (itemID) => game.stats.itemFindCount(itemID) > 0;
	const isBankFull = () => game.bank.occupiedSlots === game.bank.maximumSlots;

	// Settings Menu
	const addSubmodSettings = (submod_name, pluginConfigs) => {
		settings.section(submod_name).add(pluginConfigs);
	};

	const getSubmodSettings = (submod_name, setting_name) => {
		return settings.section(submod_name).get(setting_name);
	};

	addSubmodSettings('SEMI Core', {
		name: 'debug-logging',
		type: 'switch',
		label: 'Debug Logging',
		hint: 'Toggle debug console logging used in SEMI Mods.',
		default: false
	});

	// Report success
	log(id, 'Successfully loaded!');

	// Export our custom functions
	api({
		log,
		addSideBarModSetting,
		buildModal,
		equipmentSlotHasItem,
		equipmentEquipSlot,
		equipmentSnapshot,
		equipmentSnapshotRestore,
		equipmentDifferences,
		equipmentDifferencesRestore,
		addSubmodSettings,
		getSubmodSettings,
		isItemFound,
		isBankFull
	});
}